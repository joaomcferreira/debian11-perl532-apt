FROM debian:bullseye-slim

MAINTAINER João Miguel Ferreira <joao.miguel.c.ferreira@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y
RUN apt install -y perl

#
#cpanm, make, gcc and such are not wanted here.
#any lib*-perl that drags them must be added by
#derived images
#

COPY apt-list-a.txt /build/
RUN apt install -y $(cat /build/apt-list-a.txt)

COPY . /app
WORKDIR /app

CMD ["prove"]
